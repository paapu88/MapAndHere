from kivy.app import App
from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.core.image import Image as CoreImage
from kivy.uix.image import Image
from kivy.properties import ObjectProperty
from kivy.network.urlrequest import UrlRequest
from kivy.app import App
from kivy.uix.image import AsyncImage
from kivy.graphics.texture import Texture
import sys
import numpy as np
import urllib
import cv2
from os import listdir
from skimage import io
kv_path = './kv/'


    

class MainApp(App):
    texture = ObjectProperty()
    

    def download(self):
        img = io.imread('https://www.mapant.se/fi/tiles/4/77/37.png')
        print(img.shape)
        self.texture = Texture.create(size=(256, 256), colorfmt="rgba")
        #arr = numpy.ndarray(shape=[16, 16, 3], dtype=numpy.uint8)
        # fill your numpy array here
        data = img.tostring()
        self.texture.blit_buffer(data, bufferfmt="ubyte", colorfmt="rgba")
        

    def build(self):        
        for kv in listdir(kv_path):
            Builder.load_file(kv_path+kv)
        self.download()
        print("DL OK")

if __name__ == "__main__":
    app = MainApp()
    app.run()
    
